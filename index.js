const express = require('express');
const rssItemRouter = require('./routers/rssItemRouter');
const { port } = require('./config');

const app = express();
app.use(express.json());

app.use('/rssitem', rssItemRouter);

app.listen(port, () => console.log(`App listening on http://localhost:${port}!`));