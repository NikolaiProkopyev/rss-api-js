const Pool = require('pg').Pool;
const { connection } = require('../config')

const pool = new Pool(connection);
const queryStrings = {
    selectAll: `SELECT "id", "source", "title", "link", "date" 
                FROM "rss_item" 
                ORDER BY "id"`,
    select: `SELECT "id", "source", "title", "link", "date" 
            FROM "rss_item" 
            WHERE "id" = $1`,
    insert: `INSERT INTO "rss_item"("source", "title", "link", "date") 
            VALUES ($1, $2, $3, $4) 
            RETURNING "id", "source", "title", "link", "date"`,
    update: `UPDATE "rss_item" 
            SET "source" = $1, "title" = $2, "link" = $3, "date" = $4 
            WHERE "id" = $5 
            RETURNING "id", "source", "title", "link", "date"`,
    delete: `DELETE FROM "rss_item" 
            WHERE "id" = $1 
            RETURNING "id", "source", "title", "link", "date"`
}

async function getAll() {
    const query = await pool.query(queryStrings.selectAll);
    return query.rows;
}

async function get(id) {
    const query = await pool.query(
        queryStrings.select,
        [id]);
    if (query.rows.length < 1) {
        return null;
    }
    return query.rows[0];
}

async function post(rssItem) {
    const query = await pool.query(
        queryStrings.insert,
        [rssItem.source, rssItem.title, rssItem.link, rssItem.date]);
    if (query.rows.length < 1) {
        return null;
    }
    return query.rows[0];
}

async function put(id, rssItem) {
    const query = await pool.query(
        queryStrings.update,
        [rssItem.source, rssItem.title, rssItem.link, rssItem.date, id]);
    if (query.rows.length < 1) {
        return null;
    }
    return query.rows[0];
}

async function remove(id) {
    const query = await pool.query(
        queryStrings.delete,
        [id]);
    if (query.rows.length < 1) {
        return null;
    }
    return query.rows[0];
}

module.exports = { getAll, get, post, put, remove }