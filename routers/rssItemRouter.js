const Router = require('express').Router;
const rssItemRepository = require('../repositories/rssItemRepository')

const rssItemRouter = Router();
rssItemRouter.route('/')
    .get(async function(req, res) {
        try {
            res.send(await rssItemRepository.getAll());
        } catch (err) {
            console.error(err);
            res.status(500).send();
        }
    })
    .post(async function(req, res) {
        try {
            const rssItem = {
                source: parseInt(req.body.source),
                title: req.body.title,
                link: req.body.link,
                date: new Date(req.body.date)
            };
            res.send(await rssItemRepository.post(rssItem));
        } catch (err) {
            console.error(err);
            res.status(500).send();
        }
    });
rssItemRouter.route('/:id')
    .get(async function(req, res) {
        try {
            const id = parseInt(req.params.id);
            res.send(await rssItemRepository.get(id));
        } catch (err) {
            console.error(err);
            res.status(500).send();
        }
    })
    .put(async function (req, res){
        try {
            const id = parseInt(req.params.id);
            const rssItem = {
                source: parseInt(req.body.source),
                title: req.body.title,
                link: req.body.link,
                date: new Date(req.body.date)
            };
            res.send(await rssItemRepository.put(id, rssItem));
        } catch (err) {
            console.error(err);
            res.status(500).send();
        }
    })
    .delete(async function (req, res){
        try {
            const id = parseInt(req.params.id);
            res.send(await rssItemRepository.remove(id));
        } catch (err) {
            console.error(err);
            res.status(500).send();
        }
    });

module.exports = rssItemRouter;